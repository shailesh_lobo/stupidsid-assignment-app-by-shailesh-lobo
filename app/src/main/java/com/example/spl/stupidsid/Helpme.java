package com.example.spl.stupidsid;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by spl on 5/27/16.
 */
public class Helpme {

    static String TAG="SS:Helpme";
    static Context context;
    public Helpme(Context c){
        this.context=c;
    }

    public static void rptErr(String Category,String Errortype){
        if (Errortype.length()!=0){
            Toast.makeText(context,
                    Errortype, Toast.LENGTH_SHORT).show();
        }
        //TODO google analytics error and exception
        Log.d(TAG+"SS:reportErr",Category);
    }

    public static String loadJSONFromAsset(String fileName) {
        System.out.print("ss: loadJsn");
        String json = null;
        try {

            InputStream is = context.getAssets().open(fileName);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return ex.getMessage();
        }
        return json;

    }
}

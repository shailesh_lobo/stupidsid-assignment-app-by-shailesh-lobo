package com.example.spl.stupidsid;

import java.util.ArrayList;

/**
 * Created by spl on 6/11/16.
 */
public class University {
    String name;
    ArrayList<Course> courseList;

    public University(String name){
        this.name=name;
        courseList=new ArrayList<>();
    }

    public String getUniversityName(){
        return name;
    }

    public void addCourse(Course courseName){
        courseList.add(courseName);
    }

    public ArrayList<Course> getCourseList(){
        return courseList;
    }

    public ArrayList<String> getCourseListToString(){
        ArrayList<String> temp=new ArrayList<>();
        for (Course course:courseList){
            temp.add(course.getCourseName());
        }
        return temp;
    }
}

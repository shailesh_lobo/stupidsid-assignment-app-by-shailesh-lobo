package com.example.spl.stupidsid;

import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class QuestionPaperActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_paper);
        View view=findViewById(R.id.viewid);

        SharedPreferences studentInfo= getSharedPreferences("studentInfo",MODE_PRIVATE);
        String Url=studentInfo.getString("imageUrl","asdfasfd");

        Snackbar.make(view, Url, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }
}

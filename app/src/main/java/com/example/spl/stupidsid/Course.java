package com.example.spl.stupidsid;

import java.util.ArrayList;

/**
 * Created by spl on 6/11/16.
 */
public class Course {

    String coursename;
    ArrayList<Semester> semesterList;

    public Course(String coursename){
        this.coursename=coursename;
        semesterList=new ArrayList<>();
    }

    public String getCourseName(){
        return coursename;
    }

    public void addSemester(Semester semesterName){
        semesterList.add(semesterName);
    }

    public ArrayList<Semester> getSemesterList(){
        return semesterList;
    }

    public ArrayList<String> getSemesterListToString(){
        ArrayList<String> temp=new ArrayList<>();
        for (Semester semester:semesterList){
            temp.add(semester.getSemesterName());
        }
        return temp;
    }
}

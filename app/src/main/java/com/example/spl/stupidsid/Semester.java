package com.example.spl.stupidsid;

import java.util.ArrayList;

/**
 * Created by spl on 6/11/16.
 */
public class Semester {

    String semesterName;
    ArrayList<Subject> subjectList;

    public Semester(String semesterName){
        this.semesterName=semesterName;
        subjectList=new ArrayList<>();
    }

    public String getSemesterName(){
        return semesterName;
    }

    public void addSubject(Subject subjectName){
        subjectList.add(subjectName);
    }

    public ArrayList<Subject> getSubjectList(){
        return subjectList;
    }

    public ArrayList<String> getSubjectListToString() {
        ArrayList<String> temp=new ArrayList<>();
        for (Subject subject:subjectList){
            temp.add(subject.getSubjectName());
        }
        return temp;
    }
}

package com.example.spl.stupidsid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class ListingActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    ArrayList<String> listToShow;
    private RecyclerView.Adapter mAdapter;
    private int currentLevel;
    private String TAG="SS:ListingActivity";
    Intent intent;
    ArrayList<University> data;
    int[] choiceArray;
    SharedPreferences studentInfoPref;SharedPreferences.Editor edit;
    String[] changeBack={"University","Course","Semester","Subject","Year"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);
        currentLevel = getIntent().getExtras().getInt("level");

        getSupportActionBar().setTitle("Select "+changeBack[currentLevel]);

        intent = new Intent(getApplicationContext(), ListingActivity.class);
        /*
        * 0- Select University Listing Page
        * 1- Select Course  Listing Page
        * 2- Select Semester  Listing Page
        * 3- Select Subject  Listing Page
        *
        * */
        studentInfoPref=getSharedPreferences("studentInfo",MODE_PRIVATE);
        edit=studentInfoPref.edit();

        data=((MyApp) getApplicationContext()).getData();



        listToShow=new ArrayList<>();

        choiceArray=new int[currentLevel];
        for (int i=0;i<currentLevel;i++){
            choiceArray[i]=studentInfoPref.getInt(String.valueOf(i),0);
            Log.d(TAG,"choiceArray["+i+"]is"+String.valueOf(choiceArray[i]));
        }


        switch (currentLevel){
            case 0:
                Log.d(TAG,"case 0");
                for (int i=0;i<MainActivity.list.size();i++) {
                    Log.d(TAG,"size of data "+data.size());
                    Log.d(TAG,""+data.get(0).getUniversityName());
                    listToShow.add(data.get(i).getUniversityName());
                }
                break;
            case 1:
                Log.d(TAG,"case 1");
                listToShow=data.get(choiceArray[0]).getCourseListToString();
                break;
            case 2:
                Log.d(TAG,"case 2");
                Log.d(TAG,"c "+choiceArray[0]+" "+choiceArray[1]);
                listToShow=data.get(choiceArray[0]).getCourseList().get(choiceArray[1]).getSemesterListToString();
                break;
            case 3:
                Log.d(TAG,"case 3");
                Log.d(TAG,"c "+choiceArray[0]+" "+choiceArray[1]+" "+choiceArray[2]);
                listToShow=data.get(choiceArray[0]).getCourseList().get(choiceArray[1]).getSemesterList().get(choiceArray[2])
                        .getSubjectListToString();
                break;
            case 4:
                Log.d(TAG,"case 4");
                listToShow=data.get(choiceArray[0]).getCourseList().get(choiceArray[1]).getSemesterList().get(choiceArray[2])
                        .getSubjectList().get(choiceArray[3]).getYearList();
                break;
        }

        ///putAllExtrasAgain
        for (int i=0;i<currentLevel;i++) {
            Log.d(TAG,"puttingExtraa" +String.valueOf(i)+"for"+choiceArray[i]);
            intent.putExtra(String.valueOf(i), choiceArray[i]);
        }
        //
        dispArrayList(listToShow);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new MyListAdapter(listToShow,getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                if (currentLevel<4) {
                    System.out.println(position + " is selected!");
                    intent.putExtra("level", currentLevel + 1);
                    String s = String.valueOf(currentLevel);
                    intent.putExtra(s, position);
                    edit.putInt(s,position);
                    edit.apply();
                    Log.d(TAG,"putting "+s+" for "+ position);
                    startActivity(intent);
                }
                else if (currentLevel==4){
                    intent = new Intent(getApplicationContext(), QuestionPaperActivity.class);
                    String UniName=data.get(choiceArray[0]).getUniversityName();
                    String CourseName=data.get(choiceArray[0]).getCourseList().get(choiceArray[1])
                            .getCourseName();
                    String SemName=data.get(choiceArray[0]).getCourseList().get(choiceArray[1])
                            .getSemesterList().get(choiceArray[2]).getSemesterName();
                    String SubjectName=data.get(choiceArray[0]).getCourseList().get(choiceArray[1])
                            .getSemesterList().get(choiceArray[2]).getSubjectList()
                            .get(choiceArray[3]).getSubjectName();
                    String Year=data.get(choiceArray[0]).getCourseList().get(choiceArray[1])
                            .getSemesterList().get(choiceArray[2]).getSubjectList()
                            .get(choiceArray[3]).getYearList().get(position);
                    String Url=UniName+"/"+CourseName+"/"+SemName+"/"
                            +SubjectName+"/"+Year;
                    edit.putString("imageUrl",Url);
                    edit.apply();

                    startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                System.out.println(position+ " is selected! is long pressed");
                view.setSelected(true);
            }
        }));


        recyclerView.setAdapter(mAdapter);

        changeButtonText();
    }

    private void changeButtonText() {

        TextView b= (TextView) findViewById(R.id.textView2);
        if (currentLevel>0 && currentLevel<4){
            b.setText("Change "+ changeBack[currentLevel-1]);
        }
    }

    public interface ClickListener {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ListingActivity.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ListingActivity.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    void dispArrayList(ArrayList<String> arrayList){
        for (String s:arrayList){
            Log.d(TAG,"content is "+s);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*getMenuInflater().inflate(R.menu.main, menu);*/
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    public void onClickBack(View view){

        if (currentLevel!=0){
            intent = new Intent(getApplicationContext(), ListingActivity.class);
            intent.putExtra("level",currentLevel-1);
        }
        else{
            intent = new Intent(getApplicationContext(), MainActivity.class);
        }
        finish();
        startActivity(intent);
    }
}

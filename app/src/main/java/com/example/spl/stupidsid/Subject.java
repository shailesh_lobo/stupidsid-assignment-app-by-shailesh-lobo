package com.example.spl.stupidsid;

import java.util.ArrayList;

/**
 * Created by spl on 6/11/16.
 */
public class Subject {

    String subjectName;
    ArrayList<String> YearList;

    public Subject(String subjectName){
        this.subjectName=subjectName;
        YearList=new ArrayList<>();
    }

    public String getSubjectName(){
        return subjectName;
    }

    public void addYearArrayList(ArrayList<String> arr){
        YearList=arr;
    }

    public ArrayList<String> getYearList(){
        return YearList;
    }
}

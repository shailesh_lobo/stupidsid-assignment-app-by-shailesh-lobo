package com.example.spl.stupidsid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private String TAG="SS:MainActivity";
    Helpme helpme;
    public static ArrayList<University> list;
    public static ArrayList<University> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        helpme=new Helpme(getApplicationContext());

        if (list==null){
            list= fetchCompleteList();
            data = makeOrderedList(list);
            ((MyApp) getApplicationContext()).setData(data);
        }
        Log.d(TAG,list.get(1).getCourseList().get(2).getSemesterList().get(3).getSubjectList().get(3).getYearList().toString());
    }

    /**
    *
    * This function is used to Order ArrayLists  in Alphabetical Order
    *
    * */
    private ArrayList<University> makeOrderedList(ArrayList<University> list) {
        data=new ArrayList<>();
        for (University uni:list){
            University uni2= new University(uni.getUniversityName());
            ArrayList<Course> courseList=uni.getCourseList();
            for (Course course:courseList){
                Course course2= new Course(course.getCourseName());
                ArrayList<Semester> semList= course.getSemesterList();
                Collections.sort(semList, new Comparator<Semester>() {
                    @Override
                    public int compare(Semester lhs, Semester rhs) {
                        return lhs.getSemesterName().compareTo(rhs.getSemesterName());
                    }
                });
                for (Semester sem:semList){
                    Semester sem2= new Semester(sem.getSemesterName());
                    ArrayList<Subject> subjectList= sem.getSubjectList();
                    Collections.sort(subjectList, new Comparator<Subject>() {
                        @Override
                        public int compare(Subject lhs, Subject rhs) {
                            return lhs.getSubjectName().compareTo(rhs.getSubjectName());
                        }
                    });
                    for (Subject subject:subjectList){
                        Subject subject2=new Subject(subject.getSubjectName());
                        ArrayList<String> year=subject.getYearList();
                        subject2.addYearArrayList(year);
                        sem2.addSubject(subject2);
                    }
                    course2.addSemester(sem2);
                }
                uni2.addCourse(course2);
            }
            data.add(uni2);
        }
        return data;
    }

    /**
    *
    * This function is used to fetch the Data of stored in the /assets/UniversityPapers.json
    * file. It fetches data and returns a <b> ArrayList</b> of University Object.
     *
     *
     *   Why Json File is used ?
     *   So that this file as be replaced by a rest api, as a rest api sends data in json file.
     *   This is useful when :  1. a university is added.
     *      `                   2. Changes inside a University.like course/subj/... change, name
     *                              change and so.on
    *
    * Note.: Each University Object has a Course Ojbect
    *       Each Course has has Semester Object , Each Semester has Subject Object and
    *       Each Subject has a Exam Name (month with year) Object
    *
    *
    *       University-> Course -> Semester -> Subject -> Exam Name
    *
    * */
    private ArrayList<University> fetchCompleteList() {
        //Fetch the complete data from UniversityPapers.json file

        ArrayList<University> UniList= new ArrayList<>();
        try {
            String tmp = helpme.loadJSONFromAsset("UniversityPapers.json");
            JSONObject data= new JSONObject(tmp);
            Iterator<String> keys =data.keys();
            List<String> UniversityList = new ArrayList<>();
            while (keys.hasNext()) {
                UniversityList.add(keys.next());
            }
            Log.d(TAG,"ss:list of universities is: "+UniversityList);
            for (int i=0;i<UniversityList.size();i++){
                String universityName=UniversityList.get(i);
                Log.d(TAG,"inside university: "+universityName);
                University university= new University(universityName);
                JSONObject uni= data.getJSONObject(universityName);
                Iterator<String> courseKeys =uni.keys();
                List<String> CourseList = new ArrayList<>();
                while (courseKeys.hasNext()) {
                    CourseList.add(courseKeys.next());
                }
                for (int j=0;j<CourseList.size();j++){
                    String courseName=CourseList.get(j);
                    Log.d(TAG,"inside course Name is  "+courseName);
                    Course course= new Course(courseName);
                    JSONObject courseJson= uni.getJSONObject(courseName);
                    Iterator<String> SemKeys =courseJson.keys();
                    List<String> semList = new ArrayList<>();
                    while (SemKeys.hasNext()) {
                        semList.add(SemKeys.next());
                    }
                    for (int k=0;k<semList.size();k++){
                        String semName=semList.get(k);
                        Log.d(TAG,"inside sem :  "+semName);
                        Semester semester= new Semester(semName);
                        JSONObject semJson= courseJson.getJSONObject(semName);
                        Iterator<String> SubKeys =semJson.keys();
                        List<String> subjectList = new ArrayList<>();
                        while (SubKeys.hasNext()) {
                            subjectList.add(SubKeys.next());
                        }
                        Log.d(TAG,"size is "+subjectList.size());
                        for (int l=0;l<subjectList.size();l++) {
                            Log.d(TAG,"inside"+l+" sub: ");
                            String subName = subjectList.get(l);
                            Log.d(TAG,subName);
                            Subject subject = new Subject(subName);
                            JSONArray yearList =semJson.getJSONArray(subName);
                            ArrayList<String> yearArrayList= new ArrayList<>();

                            if (yearList != null) {
                                for (int m=0;m<yearList.length();m++){
                                    yearArrayList.add(yearList.get(m).toString());
                                }
                            }
                            Log.d(TAG,"added sub3: "+subName);
                            subject.addYearArrayList(yearArrayList);

                            semester.addSubject(subject);try{}
                            catch(Exception e){Log.d(TAG,"error is : "+e.toString());}
                            Log.d(TAG,"added sub: "+subName);
                        }
                        Log.d(TAG,"added sub: "+"done with for loop");
                        try{
                        course.addSemester(semester);
                            Log.d(TAG,"added semester ");
                        }
                        catch(Exception e){Log.d(TAG,"error while adding sem to course is : "+e.toString());}
                    }
                    university.addCourse(course);
                    Log.d(TAG,"added Course ");
                }
                UniList.add(university);
                Log.d(TAG,"added UNi ");
            }

        }
        catch(Exception e){
            e.printStackTrace();
            Helpme.rptErr("---------------------DANGER-------------------Fetc"+e.getClass().toString(),"");
        }
        return UniList;
    }

    @Override
    public void onBackPressed() {
        System.out.print(TAG+"back pressed");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*getMenuInflater().inflate(R.menu.main, menu);*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent = new Intent(getApplicationContext(), ListingActivity.class);
        if (id == R.id.nav_camera) {
            SharedPreferences studentInfo= getSharedPreferences("studentInfo",MODE_PRIVATE);
            if (studentInfo.contains("2")){
                intent.putExtra("level", 3);
            }
            else if (studentInfo.contains("1")){
                intent.putExtra("level", 2);
            }
            else if (studentInfo.contains("0")){
                intent.putExtra("level", 1);
            }
            else {
                intent.putExtra("level", 0);
            }
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
